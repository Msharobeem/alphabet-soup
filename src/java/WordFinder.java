import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class WordFinder {
    public static void main(String[] args) {
        try {
            BufferedReader inputReader = new BufferedReader(new FileReader("./test/data"));
            String dimensions = inputReader.readLine();
            String[] dimensionArr = dimensions.split("x");
            int rows = Integer.parseInt(dimensionArr[0]);

            List<List<String>> grid = new ArrayList<>();
            for (int i = 0; i < rows; i++) {
                String line = inputReader.readLine();
                String[] chars = line.split(" ");
                List<String> row = new ArrayList<>();
                for (String c : chars) {
                    row.add(c.trim());
                }
                grid.add(row);
            }

            List<String> words = new ArrayList<>();
            String word;
            while ((word = inputReader.readLine()) != null) {
                words.add(word.trim());
            }

            for (String w : words) {
                List<Integer> result = findWord(w, grid);
                if (result != null) {
                    System.out.println(w + " " + result.get(0) + ": " + result.get(1) + " " + result.get(2) + ": " + result.get(3));
                } else {
                    System.out.println(w + " Not Found");
                }
            }

            inputReader.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static List<Integer> findWord(String word, List<List<String>> grid) {
        int rows = grid.size();
        int cols = grid.get(0).size();

        // Horizontally
        for (int row = 0; row < rows; row++) {
            for (int col = 0; col < cols - word.length() + 1; col++) {
                List<String> subList = grid.get(row).subList(col, col + word.length());
                if (isWordMatch(subList, word) || isWordMatch(reverseList(subList), word)) {
                    return List.of(row, col, row, col + word.length() - 1);
                }
            }
        }

        // Vertically
        for (int col = 0; col < cols; col++) {
            for (int row = 0; row < rows - word.length() + 1; row++) {
                List<String> subList = new ArrayList<>();
                for (int i = 0; i < word.length(); i++) {
                    subList.add(grid.get(row + i).get(col));
                }
                if (isWordMatch(subList, word)) {
                    return List.of(row, col, row + word.length() - 1, col);
                }
            }
        }

        // Diagonally forward
        for (int row = 0; row < rows - word.length() + 1; row++) {
            for (int col = 0; col < cols - word.length() + 1; col++) {
                List<String> subList = new ArrayList<>();
                for (int i = 0; i < word.length(); i++) {
                    subList.add(grid.get(row + i).get(col + i));
                }
                if (isWordMatch(subList, word)) {
                    return List.of(row, col, row + word.length() - 1, col + word.length() - 1);
                }
            }
        }

        // Diagonally backward
        for (int row = word.length() - 1; row < rows; row++) {
            for (int col = 0; col < cols - word.length() + 1; col++) {
                List<String> subList = new ArrayList<>();
                for (int i = 0; i < word.length(); i++) {
                    subList.add(grid.get(row - i).get(col + i));
                }
                if (isWordMatch(subList, word)) {
                    return List.of(row, col, row - word.length() + 1, col + word.length() - 1);
                }
            }
        }

        return null;
    }

    private static boolean isWordMatch(List<String> subList, String word) {
        for (int i = 0; i < word.length(); i++) {
            if (!subList.get(i).equals(String.valueOf(word.charAt(i)))) {
                return false;
            }
        }
        return true;
    }

    private static List<String> reverseList(List<String> list) {
        List<String> reversed = new ArrayList<>();
        for (int i = list.size() - 1; i >= 0; i--) {
            reversed.add(list.get(i));
        }
        return reversed;
    }
}
