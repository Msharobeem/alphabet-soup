def find_word(word, grid):
    rows = len(grid)
    cols = len(grid[0])

    # horizontally
    for row in range(rows):
        for col in range(cols - len(word) + 1):
            if grid[row][col:col+len(word)] == list(word) or grid[row][col:col+len(word)][::-1] == list(word):
                return (row, col, row, col + len(word) - 1)

    # vertically
    for col in range(cols):
        for row in range(rows - len(word) + 1):
            if all(grid[row+i][col] == word[i] for i in range(len(word))):
                return (row, col, row + len(word) - 1, col)

    # diagonally forward
    for row in range(rows - len(word) + 1):
        for col in range(cols - len(word) + 1):
            if all(grid[row+i][col+i] == word[i] for i in range(len(word))):
                return (row, col, row + len(word) - 1, col + len(word) - 1)

    # diagonally backward
    for row in range(len(word) - 1, rows):
        for col in range(cols - len(word) + 1):
            if all(grid[row-i][col+i] == word[i] for i in range(len(word))):
                return (row, col, row - len(word) + 1, col + len(word) - 1)

    return None
def main():
    input_file = open('./test/data', 'r')
    # get dimentions
    rows, cols = input_file.readline().split('x')
    grid = []
    # read chars and construct grid
    for _ in range(int(rows)):
        grid.append([x.strip() for x in input_file.readline().split(' ')])
    #read words to find
    words = [element.strip() for element in input_file.readlines()]
    
    for word in words:
        result = find_word(word, grid)
        if result:
            print(word, result[0],':', result[1], ' ', result[2], ':', result[3])
        else:
            print(word, 'Not Found')
if __name__ == '__main__':
    main()
